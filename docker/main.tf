data "docker_registry_image" "nginx" {
  name = "nginx:latest"
}

resource "docker_image" "nginx" {
  name = data.docker_registry_image.nginx.name
  pull_triggers = ["${data.docker_registry_image.nginx.sha256_digest}"]
}

resource "docker_container" "nginx-server" {
  name = "nginx-server"
  image = docker_image.nginx.latest
  
  ports {
    internal = 80
    external = 80
  }
  
}
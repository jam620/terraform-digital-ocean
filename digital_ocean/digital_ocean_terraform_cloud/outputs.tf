output "ip_publica" {
    value = digitalocean_droplet.devsecops.*.ipv4_address
}

output "ip_balanceador" {
    value = "${join("", compact(concat(digitalocean_loadbalancer.devsecops.*.ip, list(""))))}"
}
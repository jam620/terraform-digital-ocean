variable "do_token" {} # Token de acceso a Digital ocean

variable "nombre" {
  description = "Nombre del servidor en DO"
}

variable "region" {
  default = "nyc3"
}

variable "tamano" {
  default = "s-1vcpu-1gb"
} 

variable "conteo_droplets" {
  
  default = "2"
  type = number
  description = "Cantidad de maquinas a crear"
}
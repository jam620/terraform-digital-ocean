
provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_tag" "default_tag" {
  count = var.conteo_droplets > 0 ? 1 : 0

  name = "DEFAULT:${var.nombre}"
}

resource "digitalocean_droplet" "devsecops" {
    count = var.conteo_droplets

    image  = "ubuntu-20-04-x64"
    name   = format("${var.nombre}-web-%03d", count.index + 1)
    region = var.region
    size   = var.tamano
    private_networking = true

    tags   =  digitalocean_tag.default_tag.*.name
    
}

resource "digitalocean_loadbalancer" "devsecops" {
  count = var.conteo_droplets > 0 ? 1 : 0

  name = "web-lb-devsecops-Gitlab"
  region = var.region

  forwarding_rule {
    entry_port = 80
    entry_protocol = "http"

    target_port = 80
    target_protocol = "http"
  }

  healthcheck {
    port = 22
    protocol = "tcp"
  }
  
   droplet_tag = element(digitalocean_tag.default_tag.*.name, count.index)
}

